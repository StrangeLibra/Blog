﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Entities;
using App.Entities.Dtos;

namespace App.IServices
{
    public interface IFriendLinkLogic : IBaseLogic<FriendLink>
    {
        /// <summary>
        /// 新增/编辑友情链接
        /// </summary>
        /// <param name="link">友情链接信息</param>
        /// <returns></returns>
        OperateResult Save(FriendLink link);
    }
}
