﻿using App.Core;
using App.Entities;

namespace App.IRepository
{
    public interface ISysRoleRepository : IBaseRepository<SysRole>
    {

    }
}
