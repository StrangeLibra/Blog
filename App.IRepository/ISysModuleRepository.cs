﻿using App.Core;
using App.Entities;

namespace App.IRepository
{
    public interface ISysModuleRepository : IBaseRepository<SysModule>
    {

    }
}
