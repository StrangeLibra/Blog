﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Entities;
using App.Core;

namespace App.IRepository
{
    public interface ICategoryInfoRepository : IBaseRepository<CategoryInfo>
    {
    }
}
