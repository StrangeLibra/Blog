﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Core;
using App.Entities;

namespace App.IRepository
{
    public interface ITimeLineRepository : IBaseRepository<TimeLine>
    {

    }
}
