﻿using App.IRepository;
using App.Entities;
using App.Core;

namespace App.Repository
{
    public class SysLoginLogRepository : BaseRepository<SysLoginLog>, ISysLoginLogRepository
    {
    }
}
